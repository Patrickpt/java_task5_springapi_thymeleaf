## Java Task 5 - Spring API and Thymeleaf

The application has two parts, and can be found at:
https://java-task5-experis-noroff.herokuapp.com/
### Web application 
The web app is built with Thymeleaf. It contains
 two views; a home page that contains a search bar and displays 5 random songs, artists and albums.
 And a search result view that displays the results from the search.
 
### Rest Api
#### Endpoints
Example uses of the endpoints can be found in the documentation directory
##### Get all customers
* Method: `GET` 
* URL: /api/customers
* Description: Returns a simplified version of all stored customers

##### Add customer
* Method: `POST`
* URL: /api/customers
* Description: Adds a new customer to the database

##### Get most popular genre
* Method: `GET`
* URL: /api/customers/:id/popular/genre
* Description: Returns the most popular genre of the customer specified by the id. In the case that the customer has several genres that are as popular, all will be returned.

##### Get highest spenders
* Method: `GET`
* URL: /api/customers/highestSpenders
* Description: Returns an ordered list of the highest spending customers

##### Get customers by country
* Method: `GET`
* URL: /api/customers/country
* Description: Returns an ordered list of countries with the most registered customers

##### Update customer
* Method: `PUT`
* URL: /api/customers/:id
* Description: Updates the customer specified by the id