package com.noroff.JavaTask5.data_access;

import com.noroff.JavaTask5.models.CustomerShort;
import com.noroff.JavaTask5.models.Song;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class SongViewRepository {
    private String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;

    public ArrayList<String> getRandomSongs(int amount){
        ArrayList<String> randomSongs = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement statement = conn.prepareStatement("SELECT Name FROM Track ORDER BY RANDOM() LIMIT ?");
            statement.setInt(1, amount);
            ResultSet res = statement.executeQuery();
            while(res.next()){
                randomSongs.add(res.getString("Name"));
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                conn.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return randomSongs;
    }

    public ArrayList<String> getRandomArtists(int amount){
        ArrayList<String> randomArtists = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement statement = conn.prepareStatement("SELECT Name FROM Artist ORDER BY RANDOM() LIMIT ?");
            statement.setInt(1, amount);
            ResultSet res = statement.executeQuery();
            while(res.next()){
                randomArtists.add(res.getString("Name"));
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                conn.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return randomArtists;
    }

    public ArrayList<String> getRandomGenres(int amount){
        ArrayList<String> randomGenres = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement statement = conn.prepareStatement("SELECT Name FROM Genre ORDER BY RANDOM() LIMIT ?");
            statement.setInt(1, amount);
            ResultSet res = statement.executeQuery();
            while(res.next()){
                randomGenres.add(res.getString("Name"));
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                conn.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return randomGenres;
    }

    public ArrayList<Song> getSongBySearch(String searchString){
        ArrayList<Song> songs = new ArrayList<>();
        try{
            conn = DriverManager.getConnection(URL);
            PreparedStatement statement = conn.prepareStatement("SELECT Track.Name, A2.Name, Title, G.Name from Track JOIN Album A on A.AlbumId = Track.AlbumId JOIN Artist A2 on A2.ArtistId = A.ArtistId JOIN Genre G on G.GenreId = Track.GenreId WHERE Track.Name LIKE ?");
            statement.setString(1, "%"+searchString+"%");
            ResultSet res = statement.executeQuery();
            while(res.next()){
                songs.add(new Song(
                        res.getString(1),
                        res.getString(2),
                        res.getString(3),
                        res.getString(4)
                ));
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                conn.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return songs;
    }
}
