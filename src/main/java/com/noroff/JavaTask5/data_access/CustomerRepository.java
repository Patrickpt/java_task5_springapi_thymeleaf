package com.noroff.JavaTask5.data_access;

import com.noroff.JavaTask5.models.Customer;
import com.noroff.JavaTask5.models.CustomerShort;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class CustomerRepository {
    private String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;


    /*
    Methods to manipulate database go here
     */
    public ArrayList<CustomerShort> getAllCustomers(){
        ArrayList<CustomerShort> customers = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement statement = conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone FROM Customer");
            ResultSet res = statement.executeQuery();
            while(res.next()){
                customers.add(new CustomerShort(
                        res.getInt("CustomerId"),
                        res.getString("FirstName"),
                        res.getString("LastName"),
                        res.getString("Country"),
                        res.getString("PostalCode"),
                        res.getString("Phone")
                ));
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                conn.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        return customers;
    }

    public Boolean addCustomer(Customer customer){
        Boolean success = false;
        try{
            conn = DriverManager.getConnection(URL);
            //Might need to add id?
            PreparedStatement statement = conn.prepareStatement("INSERT INTO Customer(FirstName, LastName, Company, Address, City, State, Country, PostalCode, Phone, Fax, Email, SupportRepId) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            statement.setString(1, customer.getFirstName());
            statement.setString(2, customer.getLastName());
            statement.setString(3, customer.getCompany());
            statement.setString(4, customer.getAddress());
            statement.setString(5, customer.getCity());
            statement.setString(6, customer.getState());
            statement.setString(7, customer.getCountry());
            statement.setString(8, customer.getPostalCode());
            statement.setString(9, customer.getPhoneNumber());
            statement.setString(10, customer.getFax());
            statement.setString(11, customer.getEmail());
            statement.setInt(12, customer.getSupportRepId());
            System.out.println(customer.getFirstName());
            int res = statement.executeUpdate();
            success = (res != 0);
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            try {
                conn.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return success;
    }

    public boolean updateCustomer(Customer customer, int id){
        Boolean success = false;
        System.out.println(customer);
        System.out.println(id);
        try{
            conn = DriverManager.getConnection(URL);
            //Might need to add id?
            PreparedStatement statement = conn.prepareStatement("UPDATE Customer SET FirstName=?, LastName=?, Company=?, Address=?, City=?, State=?, Country=?, PostalCode=?, Phone=?, Fax=?, Email=?, SupportRepId=? WHERE CustomerId=?");
            statement.setString(1, customer.getFirstName());
            statement.setString(2, customer.getLastName());
            statement.setString(3, customer.getCompany());
            statement.setString(4, customer.getAddress());
            statement.setString(5, customer.getCity());
            statement.setString(6, customer.getState());
            statement.setString(7, customer.getCountry());
            statement.setString(8, customer.getPostalCode());
            statement.setString(9, customer.getPhoneNumber());
            statement.setString(10, customer.getFax());
            statement.setString(11, customer.getEmail());
            statement.setInt(12, customer.getSupportRepId());
            statement.setInt(13, id);
            System.out.println(customer.getFirstName());
            int res = statement.executeUpdate();
            success = (res != 0);
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            try {
                conn.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return success;
    }

    public ArrayList<String> countCustomersByCountry(){
        ArrayList<String> customersByCountry = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement statement = conn.prepareStatement("SELECT Country, COUNT(*) FROM Customer GROUP BY Country ORDER BY COUNT(*) DESC");
            ResultSet res = statement.executeQuery();
            while(res.next()){
                customersByCountry.add(res.getString("Country") + ": " + res.getInt("COUNT(*)"));
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                conn.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return customersByCountry;
    }

    public ArrayList<String> getHighestSpenders(){
        ArrayList<String> highestSpenders = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement statement = conn.prepareStatement(" SELECT Customer.FirstName, Customer.LastName, SUM(Total) AS Sum FROM Invoice JOIN Customer ON Invoice.CustomerId = Customer.CustomerId GROUP BY Invoice.CustomerId ORDER BY Sum DESC");
            ResultSet res = statement.executeQuery();
            while(res.next()){
                highestSpenders.add(res.getString("FirstName") + " " + res.getString("LastName") + ": " + res.getInt("Sum"));
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                conn.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return highestSpenders;
    }

    public ArrayList<String> getMostPopularGenre(int id){
        ArrayList<String> genres = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement statement = conn.prepareStatement("SELECT genreName, genreCount FROM(" +
                    " SELECT Genre.Name as genreName, COUNT(*) as genreCount" +
                    " FROM Track" +
                    " INNER JOIN Genre ON Genre.GenreId = Track.GenreId " +
                    " INNER JOIN InvoiceLine ON InvoiceLine.TrackId = Track.TrackId" +
                    " INNER JOIN Invoice ON Invoice.InvoiceId = InvoiceLine.InvoiceId" +
                    " INNER JOIN Customer ON Customer.CustomerId = Invoice.CustomerId" +
                    " WHERE Customer.CustomerId=?" +
                    " GROUP BY Genre.Name )" +
                    " WHERE genreCount = ( SELECT MAX(genreCount)" +
                    " FROM ( SELECT Genre.Name as genreName, COUNT(*) as genreCount" +
                    " FROM Track" +
                    " INNER JOIN Genre ON Genre.GenreId = Track.GenreId"+
                    " INNER JOIN InvoiceLine ON InvoiceLine.TrackId = Track.TrackId"+
                    " INNER JOIN Invoice ON Invoice.InvoiceId = InvoiceLine.InvoiceId"+
                    " INNER JOIN Customer ON Customer.CustomerId = Invoice.CustomerId"+
                    " WHERE Customer.CustomerId=?" +
                    " GROUP BY Genre.Name" +
                    ")" +
                    ")"
            );
            statement.setInt(1, id);
            statement.setInt(2, id);
            ResultSet res = statement.executeQuery();
            while(res.next()){
                genres.add(res.getString(1) + " , Spent: " + res.getInt(2));
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                conn.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return genres;
    }
}
