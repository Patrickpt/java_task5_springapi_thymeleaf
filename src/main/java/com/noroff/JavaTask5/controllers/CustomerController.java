package com.noroff.JavaTask5.controllers;

import com.noroff.JavaTask5.data_access.CustomerRepository;
import com.noroff.JavaTask5.models.Customer;
import com.noroff.JavaTask5.models.CustomerShort;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class CustomerController {
    CustomerRepository customerRepository = new CustomerRepository();

    @RequestMapping(value = "/api/customers", method = RequestMethod.GET)
    public ArrayList<CustomerShort> getAllCustomers(){
        return customerRepository.getAllCustomers();
    }

    @RequestMapping(value = "/api/customers", method = RequestMethod.POST)
    public Boolean addCustomer(@RequestBody Customer customer){
        System.out.println(customer.toString());
        return customerRepository.addCustomer(customer);
    }

    @RequestMapping(value = "/api/customers/{id}", method = RequestMethod.PUT)
    public Boolean updateCustomer(@RequestBody Customer customer, @PathVariable int id){
        System.out.println(customer.toString());
        return customerRepository.updateCustomer(customer, id);
    }

    @RequestMapping(value="/api/customers/country", method = RequestMethod.GET)
    public ArrayList<String> countCustomersByCountry(){
        return customerRepository.countCustomersByCountry();
    }

    @RequestMapping(value="/api/customers/highestSpenders", method = RequestMethod.GET)
    public ArrayList<String> getHighestSpenders(){
        return customerRepository.getHighestSpenders();
    }

    @RequestMapping(value = "/api/customers/{id}/popular/genre", method = RequestMethod.GET)
    public ArrayList<String> getMostPopularGenre(@PathVariable int id){
        return customerRepository.getMostPopularGenre(id);
    }
}
