package com.noroff.JavaTask5.controllers;

import com.noroff.JavaTask5.data_access.SongViewRepository;
import com.noroff.JavaTask5.models.Song;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@Controller
public class SongViewController {
    SongViewRepository songViewRepository = new SongViewRepository();

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String homePage(Model model, @ModelAttribute String searchString){
        ArrayList<String> songs = songViewRepository.getRandomSongs(5);
        ArrayList<String> artists = songViewRepository.getRandomArtists(5);
        ArrayList<String> genres = songViewRepository.getRandomGenres(5);
        model.addAttribute("songs", songs);
        model.addAttribute("artists", artists);
        model.addAttribute("genres", genres);
        model.addAttribute("searchString", searchString);
        return "homePage";
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String search(@RequestParam(value = "search", required = false) String term, Model model ) {
        if ( term == null) {
            return "redirect:/";
        }
        if (term.length() < 3) {
            return "redirect:/";
        }
        ArrayList<Song> songs = songViewRepository.getSongBySearch(term);
        boolean noResults = (songs.size() == 0);
        model.addAttribute("term", term);
        model.addAttribute("songs", songs);
        model.addAttribute("noResults", noResults);
        return "searchResults";
    }

}
